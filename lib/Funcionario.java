package lib;

public class Funcionario {

    private String nome;
    private double total;
    private double abono;
    private String sexo;

    public void total(double salario, double venda){
        total = salario+((venda*15)/100);
    }

    public void calculoProfesor(int nivel, int horas){
        switch (nivel) {
            case 1:
                total = horas*12;
                break;

            case 2:
                total = horas*17;
                break;

            case 3:
                total = horas*25;
                break;
        }
    }

    public void salarioTipo(String tipo, double salario, double ajuste){

        if (tipo=="p") {
            total=salario+((salario*ajuste)/100);
        }
        else if(tipo=="a"){
            total=salario+ajuste;
        }

    }

    public void abono(double salario, int idade, String sexo){

        switch (sexo.toLowerCase()) {
            case "m":
                 if (idade>=30) {
                    abono = 100;
                    break;
                 } else if(idade<30){
                    abono = 30;
                    break;
                 }

            case "f":
                if (idade>=30) {
                    abono = 200;
                    break;
                } else if(idade<30){
                    abono = 80;
                    break;
                }
        }

        total=salario+abono;

    }

    public void salarioReajuste(double salario, double salarioMinimo){
        if (salario<(salarioMinimo*3)) {
            total = salario+((salario*50)/100);
        }
        else if (salario>(salarioMinimo*3)&&salario<(salarioMinimo*10)) {
            total = salario+((salario*20)/100);
        }
        else if (salario>(salarioMinimo*10)&&salario<(salarioMinimo*20)) {
            total = salario+((salario*15)/100);
        }
        else {
            total = salario+((salario*10)/100);
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAbono() {
        return abono;
    }

    public void setAbono(double abono) {
        this.abono = abono;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
}