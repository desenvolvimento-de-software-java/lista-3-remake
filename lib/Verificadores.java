package lib;

public class Verificadores {

    private String categoria;
    private String resultado;
    private String sexoStg;
    private String nome;
    private String tipo;
    private String sexo;
    private int totalM;
    private int totalF;
    private boolean ver;

    public void verificar(String veri){
        switch (veri.toLowerCase()) {
            case "s":
                    ver = true;
                break;

            case "n":
                    ver = false;
                break;
        }
    }

    public void veriSexo(String sexo){
        switch (sexo.toLowerCase()) {
            case "m":
                    sexoStg = "Masculino";
                    ver = true;
                    totalM++;
                break;

            case "f":
                    sexoStg = "Feminino";
                    ver = true;
                    totalF++;
                break;
        
            default:
                    ver = false;
                break;
        }
    }

    public void classificador(int idade){

        if (idade>=5&&idade<=7) {
            categoria = "Infantil A";
          }
          else if (idade>=8&&idade<=10) {
            categoria = "Infantil B";
          }
          else if (idade>=11&&idade<=13) {
            categoria = "juvenil A";
          }
          else if (idade>=14&&idade<=17) {
            categoria = "juvenil B";
          }
          else if (idade>=18&&idade<=25) {
            categoria = "Sênior";
          }

    }

    public void triangulo(double ladoA, double ladoB, double ladoC){

        if (ladoA<(ladoB+ladoC)) {
            ver = true;
        }
        else if (ladoB<(ladoA+ladoC)) {
            ver = true;
        }
        else if (ladoC<(ladoA+ladoB)) {
            ver = true;
        }
        else{
            ver = false;
        }

        if (ver==true) {
            tipoTriangulo(ladoA, ladoB, ladoC);
        }

    }

    public void tipoTriangulo(double ladoA, double ladoB, double ladoC){

        if(ladoA==ladoB){
            if (ladoB==ladoC) {
                tipo = "Equilátero";
            }else{
                tipo = "Isóscele";
            }
        }
        else if(ladoB==ladoC){
            tipo = "Isóscele";
        }
        else if (ladoA==ladoC) {
            tipo = "Isóscele";
        }
        else{
            tipo = "Escaleno";
        }
    }

    public void exercito(String sexo, int idade, String saude){

        switch (sexo.toLowerCase()) {
            case "m":

                if (idade>=18) {

                    switch (saude.toLowerCase()) {
                        case "b":
                            resultado = " esta apto";
                            break;
                        case "r":
                            resultado = " não esta apto";
                            break;
                    }
                    
                }

                break;
            case "f":
                resultado = " não esta apto";
                break;
        }

    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSexo() {
        return sexo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getTotalM() {
        return totalM;
    }

    public void setTotalM(int totalM) {
        this.totalM = totalM;
    }

    public int getTotalF() {
        return totalF;
    }

    public void setTotalF(int totalF) {
        this.totalF = totalF;
    }

    public boolean isVer() {
        return ver;
    }

    public void setVer(boolean ver) {
        this.ver = ver;
    }

    public String getSexoStg() {
        return sexoStg;
    }

    public void setSexoStg(String sexoStg) {
        this.sexoStg = sexoStg;
    }
    
}
