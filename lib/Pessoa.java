package lib;

public class Pessoa {
    private String idadeStg;
    private String nome;
    private double emc;

    public void EMC(String sexo, double altura, int idade){

        switch (sexo.toLowerCase()){

            case "m":
                emc = calcularPesoHomem(altura, idade);
            
            case "f":
                emc = calcularPesoMulher(altura, idade);

        }

    }

    public void idade(int idade){
            if (idade>=18) {
                idadeStg = "maior de idade";
            }
            else{
                idadeStg = "menor de idade";
            }
    }

    private static double calcularPesoHomem(double altura, int idade){

        if (altura > 170){

            if (idade <= 20){

                return (72.7 * (altura/100)) - 58;

            } 
            
            else if (idade <= 39){

                return (72.7 * (altura/100)) - 53;

            } 
            
            else{

                return (72.7 * (altura/100)) - 45;

            }

        } 
        
        else{

            if (idade <= 40){

                return (72.7 * altura) - 50;

            } 
            
            else{

                return (72.7 * altura) - 58;

            }

        }

    }

    private static double calcularPesoMulher(double altura, int idade){

        if (altura > 150) {

            return (62.1 * (altura/100)) - 44.7;

        } 
        
        else {

            if (idade >= 35){

                return (62.1 * (altura/100)) - 45;

            } 
            
            else{

                return (62.1 * (altura/100)) - 49;

            }

        }

    }

    public double getEmc() {
        return emc;
    }

    public void setEmc(double emc) {
        this.emc = emc;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdadeStg() {
        return idadeStg;
    }

    public void setIdadeStg(String idadeStg) {
        this.idadeStg = idadeStg;
    }
  
}