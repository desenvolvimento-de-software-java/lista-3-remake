package lib;

public class Aluno {

    private String nome;
    private String notaStg;
    private int matricula;
    private double nota1;
    private double nota2;
    private double nota3;
    private double total;
    private double media;

    public void Media(double nota1, double nota2, double nota3){
        media = (nota1+nota2+nota3)/3;
        stgnota(media);
    }

    public void stgnota(double media){
        if (media>=7) {
            notaStg = "aprovado";
        }

        else if (media<=5){
            notaStg = "Reprovado";
        }

        else{
            notaStg = "em Recuperação";
        }
    }

    public void MediaPonderada(double nota1, double nota2, double nota3){
        total = ((nota1*2)+(nota2*3)+(nota3*5))/10;

        if (total>=8&&total<=10) {
            notaStg = "A";
        }
        else if (total>=7&&total<8) {
            notaStg = "B";
        }
        if (total>=6&&total<7) {
            notaStg = "C";
        }
        if (total>=5&&total<6) {
            notaStg = "D";
        }
        if (total>=0&&total<5) {
            notaStg = "R";
        }
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getNota1() {
        return nota1;
    }

    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }

    public double getNota2() {
        return nota2;
    }

    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }

    public double getNota3() {
        return nota3;
    }

    public void setNota3(double nota3) {
        this.nota3 = nota3;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getNotaStg() {
        return notaStg;
    }

    public void setNotaStg(String notaStg) {
        this.notaStg = notaStg;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

}