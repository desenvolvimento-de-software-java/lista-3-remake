package lib;

public class Dinheiro {
    private double valor;
    private double total;

    public void converter(double cotacao, double dolar){
        valor = dolar*cotacao;
    }

    public void parcelas(double valor){
        Prompt.imprimir("1 x " + valor);
        Prompt.imprimir("2 x " + valor/2);
        Prompt.imprimir("3 x " + valor/3);
        Prompt.imprimir("4 x " + valor/4);
        Prompt.imprimir("5 x " + valor/5);
    }

    public void venda(double custo, double margem){
        total = custo+((custo*margem)/100);
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
}