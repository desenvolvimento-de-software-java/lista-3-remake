package lib;

public class Calculo {

    private double total;
    private double margem;
    private String margemStg;
    private String totalStg;

    public void calculoEnergia(int tipo, double consumo){
        switch (tipo) {
            case 1:
                total = consumo*0.60;
                break;

            case 2:
                total = consumo*0.48;
                break;

            case 3:
                total = consumo*1.29;
                break;
        
            default:
                break;
        }
    }

    public void margem(double precoCusto, double precoVenda){
        margem=precoVenda-precoCusto;

        if (margem>0) {
            margemStg = "produto deu lucro de R$";
            total+=margem;
        }
        else{
            margemStg = "produto deu prejuizo de R$";
            total+=margem;
        }

    }

    public void totalPL(){
        if (total>0) {
            totalStg = "Total de lucro";
        }
        else{
            totalStg = "Total deprejuizo";
        }   
    }

    public double getTotal() {
        return Math.abs(total);
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getMargem() {
        return Math.abs(margem);
    }

    public void setMargem(double margem) {
        this.margem = margem;
    }

    public String getMargemStg() {
        return margemStg;
    }

    public void setMargemStg(String margemStg) {
        this.margemStg = margemStg;
    }

    public String getTotalStg() {
        return totalStg;
    }

    public void setTotalStg(String totalStg) {
        this.totalStg = totalStg;
    }
    
}
