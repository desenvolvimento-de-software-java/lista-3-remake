package lib;

public class Carro {

    private double distancia;
    private double gasto;
    private double total;
    private double media;

    public void media(double distancia, double gasto) {
        media = distancia/gasto;
    }

    public void total(double custo){
        total+= custo;
        total+= ((custo*45)/100);
        total+= ((total*28)/100);
    }

    public void calculoTipo(double valor, String tipo){
        switch (tipo.toLowerCase()) {
            case "a":
                total = valor-((valor*25)/100);
                break;
            case "g":
                total = valor-((valor*21)/100);
                break;
            case "d":
                total = valor-((valor*14)/100);
                break;
        }
    }

    public void calculoAno(double valor, int ano){

        if (ano<=2000) {
            total = valor-((valor*12)/100);
        }

        else if(ano>2000){
            total = valor-((valor*7)/100);
        }
    }

    public double getDistancia() {
        return distancia;
    }
    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }
    public double getGasto() {
        return gasto;
    }
    public void setGasto(double gasto) {
        this.gasto = gasto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }

}