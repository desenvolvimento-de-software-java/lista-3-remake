package lib;

public class Seguradora {

    private int categoria;
    private int idade;
    private String var;
    private String nome;
    private String grupoRisco;

    public void seguro(int idade, String grupoRisco){

        if(idade<17){
            var="idadeMenos";
        }
        else if(idade>70){
            var="idadeMais";
        }
        else{
            var="seguro";
        }

            if (idade>=17&&idade<=20) {

                switch (grupoRisco.toLowerCase()) {
                    case "baixo":
                            categoria=1;
                        break;

                    case "medio":
                            categoria=2;
                        break;

                    case "alto":
                            categoria=3;
                        break;
                    default:
                            var="grupo"; 
                        break;
                }
                
            }

            else if (idade>=21&&idade<=24) {

                switch (grupoRisco.toLowerCase()) {
                    case "baixo":
                            categoria=2;
                        break;

                    case "medio":
                            categoria=3;
                        break;

                    case "alto":
                            categoria=4;
                        break;
                    default:
                            var="grupo"; 
                        break;
                }
                
            }

            else if (idade>=25&&idade<=34) {

                switch (grupoRisco.toLowerCase()) {
                    case "baixo":
                            categoria=3;
                        break;

                    case "medio":
                            categoria=4;
                        break;

                    case "alto":
                            categoria=5;
                        break;
                    default:
                            var="grupo"; 
                        break;
                }
                
            }

            else if (idade>=35&&idade<=64) {

                switch (grupoRisco.toLowerCase()) {
                    case "baixo":
                            categoria=4;
                        break;

                    case "medio":
                            categoria=5;
                        break;

                    case "alto":
                            categoria=6;
                        break;
                    default:
                        var="grupo"; 
                        break;
                }
                
            }

            else if (idade>=65&&idade<=70) {

                switch (grupoRisco.toLowerCase()) {
                    case "baixo":
                            categoria=7;
                        break;

                    case "medio":
                            categoria=8;
                        break;

                    case "alto":
                            categoria=9;
                        break;
                    default:
                        var="grupo"; 
                        break;
                }
                
            }

        }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGrupoRisco() {
        return grupoRisco;
    }

    public void setGrupoRisco(String grupoRisco) {
        this.grupoRisco = grupoRisco;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }
    
}