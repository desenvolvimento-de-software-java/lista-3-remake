/*

19. Escrever um programa que leia três valores inteiros e verifique se eles podem ser os lados de 
um triângulo. Se forem, informar qual o tipo de triângulo que eles formam: equilátero, isóscele ou 
escaleno. 
    Propriedade:  o  comprimento  de  cada  lado  de  um  triângulo  é  menor  do  que  a  soma  dos 
    comprimentos dos outros dois lados. 
    Triângulo Equilátero: aquele que tem os comprimentos dos três lados iguais; 
    Triângulo  Isóscele: aquele  que  tem  os  comprimentos  de  dois  lados  iguais.  Portanto,  todo triângulo equilátero é também isóscele; 
    Triângulo Escaleno: aquele que tem os comprimentos de seus três lados diferentes.

*/

import lib.Prompt;
import lib.Verificadores;

public class Exercicio19 {

    public static void Executar(){
        
        double ladoA = Prompt.lerDecimal("Digite o lado A do triangulo");
        double ladoB = Prompt.lerDecimal("Digite o lado B do triangulo");
        double ladoC = Prompt.lerDecimal("Digite o lado C do triangulo");

        Verificadores verificar = new Verificadores();

        verificar.triangulo(ladoA, ladoB, ladoC);

        boolean verTri = verificar.isVer();

        if (verTri==true) {
            Prompt.imprimir("Os lados informador formao um triangulo " + verificar.getTipo());
        }
        else{
            Prompt.imprimir("Os lados informados nao formao um tiangulo");
        }

    }

}