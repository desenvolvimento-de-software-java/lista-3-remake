/*

14. Faça um programa que receba o preço de custo e o preço de venda de 40 produtos. Mostre 
como resultado se houve lucro, prejuízo ou empate para cada produto. Informe média de preço de 
custo e do preço de venda.

*/

import lib.Calculo;
import lib.Prompt;

public class Exercicio14 {
 
    public static void Executar(){

        int num = Prompt.lerInteiro("Digite quantos produtos quer analizar");

        Calculo calcular = new Calculo();

        for (int i = 0; i < num; i++) {
            double precoCusto = Prompt.lerDecimal("Digite o preço de custo do produto");
            double precoVenda = Prompt.lerDecimal("Digite o preço de venda do produto");
            Prompt.separador();
                calcular.margem(precoCusto, precoVenda);
                Prompt.imprimir(calcular.getMargemStg() + calcular.getMargem());
            Prompt.separador();
        }

        calcular.totalPL();

        Prompt.separador();
        Prompt.imprimir(calcular.getTotalStg() + " R$" + calcular.getTotal());
        Prompt.separador();

    }

 }