/*

22. Faça um programa que calcule o valor da conta de luz de uma pessoa. 
Sabe-se que o cálculo da conta de luz segue a tabela abaixo: 
    Tipo de Cliente Valor do KW/h 
    1 (Residência) 0,60 
    2 (Comércio) 0,48 
    3 (Indústria) 1,29

*/

import lib.Calculo;
import lib.Prompt;

public class Exercicio22 {

    public static void Executar(){
        
        Prompt.imprimir("1-(Residência) 2-(Comércio) 3-(Indústria)");
        int tipo = Prompt.lerInteiro("Digite o numero referente ao tipo do cliente");
        double consumo = Prompt.lerDecimal("Digite o total de horas de consumo do mes");

        Calculo calcular = new Calculo();

        calcular.calculoEnergia(tipo, consumo);

        Prompt.imprimir("O valor total da conta é de R$" + calcular.getTotal());

    }

}