/*
 
3. Escrever um programa que leia o nome de um vendedor, o seu salário fixo e o total de vendas 
efetuadas por ele no mês (em dinheiro). Sabendo que este vendedor ganha 15% de comissão sobre 
suas vendas efetuadas, informar o seu nome, o salário fixo e salário no final do mês.

*/

import lib.Prompt;
import lib.Funcionario;

public class Exercicio03 {

    public static void Executar() {
        
        String nome = Prompt.lerLinha("Digite o nome do funcionario");
        double salario = Prompt.lerDecimal("Digite o valo fixo do salario do funcionario");
        double venda = Prompt.lerDecimal("Digite o valor total das venda feitas pelo funcionario");

        Funcionario funcionario = new Funcionario();

        funcionario.total(salario, venda);
        funcionario.setNome(nome);

        Prompt.imprimir("O salario final do funcionario " + funcionario.getNome() + " é de " + funcionario.getTotal());

    }

}