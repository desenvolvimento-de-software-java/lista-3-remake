/*

26. Uma seguradora possui nove categorias de seguro baseadas na idade e ocupação do segurado. 
Somente pessoas com pelo menos 17 anos e não mais que 70 anos podem adquirir apólices de 
seguro. Quanto às classes de ocupações, foram definidos três grupos de risco: baixo, médio e alto. 
A tabela abaixo fornece as categorias em função da faixa etária e do grupo de risco. Dados nome, 
idade e grupo de risco de um pretendente, determinar e imprimir seus dados e categoria. Caso a 
idade não esteja na faixa necessária, imprimir uma mensagem informando que ele não se enquadra 
em nenhuma das categorias ofertadas. 

                  Grupo de Risco 
    Idade      Baixo   Médio   Alto 
    17 a 20      1       2       3 
    21 a 24      2       3       4 
    25 a 34      3       4       5 
    35 a 64      4       5       6 
    65 a 70      7       8       9 

*/

import lib.Prompt;
import lib.Seguradora;

public class Exercicio26 {

    public static void Executar(){

        String nome = Prompt.lerLinha("Digite o nome do asegurado");
        int idade = Prompt.lerInteiro("Digite a idade do asegurado");
        Prompt.separador();
        Prompt.imprimir("    Grupos de Risco   ");
        Prompt.imprimir(" Baixo   Médio   Alto ");
        Prompt.separador();
        String risco = Prompt.lerLinha("Digite o grupo de risco");

        Seguradora seguro = new Seguradora();

        seguro.setNome(nome);
        seguro.setIdade(idade);

        seguro.seguro(idade, risco);

        if (seguro.getVar()=="seguro") {
            Prompt.imprimir("O asegurado " + seguro.getNome() + " possui idade de " + seguro.getIdade() + " e esta classificado como grau de risco " + seguro.getCategoria());
        }

        else if(seguro.getVar()=="idadeMenos"){
            Prompt.imprimir("o asegurado " + seguro.getNome() + " é menor de idade");
        }

        else if(seguro.getVar()=="idadeMais"){
            Prompt.imprimir("o asegurado " + seguro.getNome() + " possui idade superior a 70 anos");
        }
        else if(seguro.getVar()=="grupo"){
            Prompt.imprimir("Grupo nao encontrado");
        }
        
    }

}