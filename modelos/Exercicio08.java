/*

8. Escrever um programa que leia o nome e as três notas obtidas por um aluno durante o semestre. 
Calcular  a  sua  média  (aritmética),  informar  o  nome  e  sua  menção  aprovado  (media  >=  7), 
Reprovado (media <= 5) e Recuperação (media entre 5.1 a 6.9).

*/

import lib.Prompt;
import lib.Aluno;

public class Exercicio08 {

    public static void Executar(){
        
        String nome = Prompt.lerLinha("Digite o nome do aluno");
        double nota1 = Prompt.lerDecimal("Digite a primeira nota do aluno");
        double nota2 = Prompt.lerDecimal("Digite a segunda nota do aluno");
        double nota3 = Prompt.lerDecimal("Digite a terceira nota do aluno");

        Aluno aluno = new Aluno();

        aluno.setNome(nome);

        aluno.Media(nota1, nota2, nota3);

        Prompt.imprimir("O  aluno " + aluno.getNome() + " ficou com media final de: " + aluno.getMedia() + " e esta " + aluno.getNotaStg());

    }

}