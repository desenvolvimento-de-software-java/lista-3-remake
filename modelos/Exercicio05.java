/*

5.  A  Loja  Mamão  com  Açúcar  está  vendendo  seus  produtos  em  5  (cinco)  prestações  sem  juros. 
Faça um programa que receba um valor de uma compra e mostre o valor das prestações.

*/

import lib.Dinheiro;
import lib.Prompt;

public class Exercicio05 {

    public static void Executar(){

        double valor = Prompt.lerDecimal("Digite o valor total dos produtos para ver os valores das parcelas");
        
        Dinheiro dinheiro = new Dinheiro();

        dinheiro.parcelas(valor);

    }

}