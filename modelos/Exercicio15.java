/*

15. A concessionária de veículos “CARANGO” está vendendo os seus veículos com desconto. Faça 
um programa que calcule e exiba o valor do desconto e o valor a ser pago pelo cliente. O desconto 
deverá ser calculado sobre o valor do veículo de acordo com o combustível (álcool – 25%, gasolina 
–  21%  ou  diesel  –14%).  Com  valor  do  veículo  zero  encerra  entrada  de  dados.  Informe  total  de 
desconto e total pago pelos clientes.

*/

import lib.Carro;
import lib.Prompt;

public class Exercicio15 {

    public static void Executar(){
        
        double valor = Prompt.lerDecimal("Digite o valor do carro");
        Prompt.imprimir("A - alcool  G - gasolina  D - diesel");
        String tipo = Prompt.lerLinha("Digite o tipo tipo de combustivel do carro");

        Carro carro = new Carro();

        carro.calculoTipo(valor, tipo);

        Prompt.imprimir("O valor do carro com o desconto sera de " + carro.getTotal());

    }

}