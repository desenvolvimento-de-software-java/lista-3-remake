/*

25. Dado o nome de um estudante, com o respectivo número de matrícula e as três notas acima 
mencionadas,  desenvolva  um  programa  para  calcular  a  nota  final  e  a  classificação  de  cada 
estudante. A classificação é dada conforme a lista abaixo: 
Nota Final Classificação.
    [8,10] A 
    [7,8] B 
    [6,7] C 
    [5,6] D 
    [0,5] R 
    Imprima o nome do estudante, com o seu número, nota final e classificação.

*/

import lib.Aluno;
import lib.Prompt;

public class Exercicio25 {

    public static void Executar(){
        
        String nome = Prompt.lerLinha("Digite o nome do aluno");
        int matricula = Prompt.lerInteiro("Digite o numero da matricula");
        double nota1 = Prompt.lerDecimal("Digite a nota do trabalho");
        double nota2 = Prompt.lerDecimal("Digite a nota da avaliação");
        double nota3 = Prompt.lerDecimal("Digite a nota do exame final");

        Aluno aluno = new Aluno();

        aluno.setNome(nome);
        aluno.setMatricula(matricula);

        aluno.MediaPonderada(nota1, nota2, nota3);

        Prompt.imprimir("O aluno " + aluno.getNome() + " portador do matricula " + aluno.getMatricula() + " ficou com media final de " + aluno.getTotal() + " ( " + aluno.getNotaStg() + " )");

    }

}