/*

20. A escola “APRENDER” faz o pagamento de seus professores por hora/aula. Faça um programa 
que calcule e exiba o salário  de um professor. Sabe-se que o  valor da hora/aula segue a tabela 
abaixo: 
    Professor Nível 1 R$12,00 por hora/aula 
    Professor Nível 2 R$17,00 por hora/aula 
    Professor Nível 3 R$25,00 por hora/aula 

*/

import lib.Funcionario;
import lib.Prompt;

public class Exercicio20 {

    public static void Executar(){
        
        String nome = Prompt.lerLinha("Digite o nome do professor");
        Prompt.imprimir("Níveis de professores");
        Prompt.imprimir("Nível 1  Nível 2  Nível 3");
        int nivel = Prompt.lerInteiro("Digite o nivel do professor");
        int horas = Prompt.lerInteiro("Digite o tatal de horas");

        Funcionario funcionario = new Funcionario();

        funcionario.calculoProfesor(nivel, horas);

        funcionario.setNome(nome);

        Prompt.imprimir("O valor total do salario do professor " + funcionario.getNome() + " é de R$" + funcionario.getTotal());

    }

}