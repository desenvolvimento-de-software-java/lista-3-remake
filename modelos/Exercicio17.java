/*

17. Leia o nome do funcionário, seu salário e o valor do salário mínimo. Calcule o seu novo salário 
reajustado.  Escrever  o  nome  do  funcionário,  o  reajuste  e  seu  novo  salário.  Calcule  quanto  à 
empresa vai aumentar sua folha de pagamento. 

*/

import lib.Funcionario;
import lib.Prompt;

public class Exercicio17 {

    public static void Executar(){
        boolean veri;
        String var;
        double ajuste = 0;
        String tipo = "";
        
        String nome = Prompt.lerLinha("Digite o nome do funcionario");
        double salario = Prompt.lerDecimal("Digite o salario do funcionario");

        do {

            veri=false;

            Prompt.separador();

            Prompt.imprimir("P - Porcentagem sobre o salario  /  A - Acrecimo de valor");
            var = Prompt.lerLinha("Digite o tipo de acrescimo que deseja fazer");

            Prompt.separador();

            switch (var.toLowerCase()) {
                case "p":
                        ajuste = Prompt.lerDecimal("Digite a porcentagem de acrescimo ao salario do funcionario");
                        tipo="p";
                    break;

                case "a":
                        ajuste = Prompt.lerDecimal("Digite um valor para acrecentar ao salario do funcionario");
                        tipo="a";
                    break;
                default:
                        Prompt.imprimir("O tipo de reajuste não é compativel com as opções");
                        veri=true;
                    break;
            }
            
        } while (veri==true);

        Funcionario funcionario = new Funcionario();

        funcionario.salarioTipo(tipo, salario, ajuste);
        funcionario.setNome(nome);

        Prompt.imprimir("O novo salario do funcionario " + funcionario.getNome() + " sera de R$" + funcionario.getTotal());

    }

}