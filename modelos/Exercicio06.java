/*

6. Faça um programa que receba o preço de custo de um produto e mostre o valor de venda. Sabe-
se  que  o  preço  de  custo  receberá  um  acréscimo  de  acordo  com  um  percentual  informado  pelo 
usuário.

*/

import lib.Dinheiro;
import lib.Prompt;

public class Exercicio06 {

    public static void Executar(){
        
        double custo = Prompt.lerDecimal("Digite o preço de custo do produto");
        double margem = Prompt.lerDecimal("Digite a margem de lucro do produto");

        Dinheiro dinheiro = new Dinheiro();

        dinheiro.venda(custo, margem);

        Prompt.imprimir("O valor de venda do produto é de R$" + dinheiro.getTotal());

    }

}