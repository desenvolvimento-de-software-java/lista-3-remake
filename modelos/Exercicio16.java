/*

16. Escrever um programa para uma empresa que decide dar um reajuste a seus 584 funcionários 
de acordo com os seguintes critérios: 
    a) 50% para aqueles que ganham menos do que três salários mínimos; 
    b) 20% para aqueles que ganham entre três até dez salários mínimos; 
    c) 15% para aqueles que ganham acima de dez até vinte salários mínimos; 
    d) 10% para os demais funcionários.

*/

import lib.Funcionario;
import lib.Prompt;

public class Exercicio16 {

    public static void Executar() {

        String nome = Prompt.lerLinha("Digite o nome do funcionario");
        double salario = Prompt.lerDecimal("Digite o valor do salario fixo");
        double salarioMinimo = Prompt.lerDecimal("Digite o valor do salario minimo");

        Funcionario funcionario = new Funcionario();

        funcionario.setNome(nome);
        funcionario.salarioReajuste(salario, salarioMinimo);

        Prompt.imprimir("O funcionario " + funcionario.getNome() + " recebera um reajuste e passara a receber " + funcionario.getTotal());

    }

}