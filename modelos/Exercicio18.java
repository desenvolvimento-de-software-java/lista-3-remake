/*

18. Faça um programa que receba o nome a idade, o sexo e salário fixo de um funcionário. Mostre 
o nome e o salário líquido acrescido do abono conforme o sexo e a idade:
    Sexo Idade Abono 
    M >= 30    100,00 
    M <  30    50,00 
    F >= 30    200,00 
    F <  30    80,00 

*/

import lib.Funcionario;
import lib.Prompt;
import lib.Verificadores;

public class Exercicio18 {

    public static void Executar(){
        String sexo;

        Verificadores verificar = new Verificadores();
        Funcionario funcionario = new Funcionario();

        String nome = Prompt.lerLinha("Digite o nome do funcionario");
        int idade = Prompt.lerInteiro("Digite a idade do funcionario");

        do {
            Prompt.separador();
            Prompt.imprimir("M - Masculino   F - Feminino");
            sexo = Prompt.lerLinha("Digite o sexo do funcionario");

            verificar.veriSexo(sexo);

            if (verificar.isVer()==false) {
                Prompt.imprimir("Sexo digitado nao correspondente");
            }

        } while (verificar.isVer()==false);

        double salario = Prompt.lerDecimal("Digite o salario do funcionario");

        funcionario.abono(salario, idade, sexo);
        funcionario.setNome(nome);

        Prompt.imprimir("O funcionrio " + funcionario.getNome() + " recebera um total de R$" + funcionario.getTotal());

    }

}