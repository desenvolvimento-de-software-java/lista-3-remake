/*

9. Ler 80 números e ao final informar quantos número(s) est(á)ão no intervalo entre 10 (inclusive) 
e 150 (inclusive).

*/

import lib.Numeros;
import lib.Prompt;

public class Exercicio09 {

    public static void Executar(){

        int limite = Prompt.lerInteiro("Digite quantos numero deseja verificar");

        int[] numeros = new int[limite];

        Prompt.imprimir("Digite os numeros que deseja analizar");

        for (int i = 0; i < limite; i++) {

            numeros[i] = Prompt.lerInteiro();
            
        }

        Numeros numero = new Numeros();

        numero.verificarNumeros(limite, numeros);

        Prompt.imprimir("O total de numeros no intervalo entre 1 e 150 é " + numero.getTotal());

    }

}