/*

4.  Elaborar  um  programa  que  efetue  a  apresentação  do  valor  da  conversão  em  real  (R$)  de  um 
valor  lido  em  dólar  (US$).  O  programa  deverá  solicitar  o  valor  da  cotação  do  dólar  e  também  a 
quantidade de dólares disponíveis com o usuário.

*/

import lib.Prompt;
import lib.Dinheiro;

public class Exercicio04 {

    public static void Executar(){

        double cotacao = Prompt.lerDecimal("Digite a cotação atual do dolar");
        double dolar = Prompt.lerDecimal("Digite o valor que deseja Converter para real");

        Dinheiro dinheiro = new Dinheiro();

        dinheiro.converter(cotacao, dolar);

        Prompt.imprimir("O valor convertido é de R$" + dinheiro.getValor());

    }

}