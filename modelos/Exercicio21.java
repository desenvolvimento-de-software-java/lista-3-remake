/*

21. Elabore um programa que, dada a idade de um nadador. Classifique-o em uma das seguintes 
categorias: 
    Infantil A = 5 - 7 anos 
    Infantil B = 8 - 10 anos 
    juvenil A = 11- 13 anos 
    juvenil B = 14 - 17 anos 
    Sênior = 18 - 25 anos 
    Apresentar mensagem “idade fora da faixa etária” quando for outro ano não contemplado.

*/

import lib.Prompt;
import lib.Verificadores;

public class Exercicio21 {

    public static void Executar(){

        String nome = Prompt.lerLinha("Digite o nome do nadador");
        int idade = Prompt.lerInteiro("Digite a idade do nadador");
        
        Verificadores verificar = new Verificadores();

        verificar.classificador(idade);

        verificar.setNome(nome);

        Prompt.imprimir("O nadador " + verificar.getNome() + " esta classificado como " + verificar.getCategoria());

    }

}