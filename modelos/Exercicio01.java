/*

1. Escrever um programa que leia o nome de um aluno e as notas das três provas que ele obteve 
no semestre. No final informar o nome do aluno e a sua média (aritmética). 

*/

import lib.Prompt;
import lib.Aluno;

public class Exercicio01 {

    public static void Executar(){

        String nome = Prompt.lerLinha("Digite o nome do aluno");
        double nota1 = Prompt.lerDecimal("Digite a primeira nota do aluno");
        double nota2 = Prompt.lerDecimal("Digite a segunda nota do aluno");
        double nota3 = Prompt.lerDecimal("Digite a terceira nota do aluno");

        Aluno aluno = new Aluno();

        aluno.setNome(nome);

        aluno.Media(nota1, nota2, nota3);

        Prompt.imprimir("O  aluno " + aluno.getNome() + " ficou com media final de: " + aluno.getMedia());

    }

}