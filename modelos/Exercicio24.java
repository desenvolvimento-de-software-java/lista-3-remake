/*

24. Em um curso de Ciência da Computação a nota do estudante é calculada a partir de três notas 
atribuídas, respectivamente, a um trabalho de laboratório, a uma avaliação semestral e a um exame 
final. As notas variam, de 0 a 10 e a nota final é a média ponderada das três notas mencionadas. 
    A lista abaixo fornece os pesos: 
    a. Laboratório: peso 2 
    b. Avaliação semestral: peso 3 
    c. Exame final: peso 5

*/

import lib.Aluno;
import lib.Prompt;

public class Exercicio24 {

    public static void Executar(){

        String nome = Prompt.lerLinha("Digite o nome do aluno");
        double nota1 = Prompt.lerDecimal("Digite a nota do trabalho");
        double nota2 = Prompt.lerDecimal("Digite a nota da avaliação");
        double nota3 = Prompt.lerDecimal("Digite a nota do exame final");

        Aluno aluno = new Aluno();

        aluno.setNome(nome);

        aluno.MediaPonderada(nota1, nota2, nota3);

        Prompt.imprimir("O aluno " + aluno.getNome() + " ficou com media final de " + aluno.getTotal());

    }

}