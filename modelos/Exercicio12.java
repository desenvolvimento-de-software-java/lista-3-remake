/*

12.  A  concessionária  de  veículos  “CARANGO  VELHO”  está  vendendo  os  seus  veículos  com 
desconto.  Faça  um  programa  que  calcule  e  exiba  o  valor  do  desconto  e  o valor  a  ser  pago  pelo 
cliente de vários carros. O desconto deverá ser calculado de acordo com o ano do veículo. Até 2000 
- 12% e acima de 2000 - 7%. O sistema deverá perguntar se deseja continuar calculando desconto 
até que a resposta seja: “(N) Não”. Informar total de carros com ano até 2000 e total geral.

*/

import lib.Carro;
import lib.Prompt;
import lib.Verificadores;

public class Exercicio12 {

    public static void Executar(){

        Carro carro = new Carro();
        Verificadores verificar = new Verificadores();

        do {

            double valor = Prompt.lerDecimal("Digite o valor do carro");
            int ano = Prompt.lerInteiro("Digite o ano do carro");

            carro.calculoAno(valor, ano);

            Prompt.imprimir("O valor do carro com desconto é de " + carro.getTotal());
            Prompt.separador();
            
            Prompt.imprimir("Deseja analizar outro carro?");
            String veri = Prompt.lerLinha("N - não   S - sim");

            verificar.verificar(veri);
            
        } while (verificar.isVer()==true);

    }

}