/*

10. Faça um programa que receba a idade de um número finito de pessoas e mostre mensagem 
informando “maior de idade” e “menor de idade” para cada pessoa. Considerar a pessoa com 18 
anos como maior de idade.

*/

import lib.Pessoa;
import lib.Prompt;

public class Exercicio10 {

    public static void Executar(){

        Pessoa pessoa = new Pessoa();

        int num = Prompt.lerInteiro("Digite quantas pessoas deseja verificar");

        for (int i = 0; i < num; i++) {
            Prompt.separador();
            String nome = Prompt.lerLinha("Digite o nome da pessoa");
            pessoa.setNome(nome);
            int idade = Prompt.lerInteiro("Digite a idade");
            pessoa.idade(idade);
            Prompt.separador();
            Prompt.imprimir(pessoa.getNome() + " é " + pessoa.getIdadeStg());
        }

    }

}