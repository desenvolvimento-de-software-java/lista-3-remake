/*

2. Escrever um programa para determinar o consumo médio de um automóvel sendo fornecida a 
distância total percorrida pelo automóvel e o total de combustível gasto. 
 
*/

import lib.Prompt;
import lib.Carro;

public class Exercicio02 {

    public static void Executar(){

        double distancia = Prompt.lerDecimal("Digite a distancia percorrida pelo automóvel");
        double gasto = Prompt.lerDecimal("Digite o total de combustivel gasto pelo automóvel");

        Carro carro = new Carro();

        carro.media(distancia, gasto);

        Prompt.imprimir("A média de comsumo do carro é de " + carro.getMedia() + "km/l");

    }

}