/*

13. Escrever um programa que leia os dados de “N” pessoas (nome, sexo, idade e saúde) e informe 
se está apta ou não para cumprir o serviço militar obrigatório. Informe os totais.

*/

import lib.Prompt;
import lib.Verificadores;

public class Exercicio13 {

    public static void Executar(){

        Verificadores verificar = new Verificadores();
        
        int limite = Prompt.lerInteiro("Digite quantas pessoas deseja analizar");

        for (int i = 0; i < limite; i++) {
            String nome = Prompt.lerLinha("Digite o nome do participante");
            verificar.setNome(nome);
            Prompt.imprimir("M - masculino   F - feminino");
            String sexo = Prompt.lerLinha("Digite o sexo do participante");
            int idade = Prompt.lerInteiro("Digite a idade do participante");
            Prompt.imprimir("B - Boa  R - ruim");
            String saude = Prompt.lerLinha("Digite a saude do participante");

            verificar.exercito(sexo, idade, saude);

            Prompt.imprimir("O participante " + verificar.getNome() + verificar.getResultado() + "\n");

        }

    }

}