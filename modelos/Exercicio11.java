/*

11. Escrever um programa que leia o nome e o sexo de 56 pessoas e informe o nome e se ela é 
homem ou mulher. No final informe total de homens e de mulheres.

*/

import lib.Prompt;
import lib.Verificadores;

public class Exercicio11 {

    public static void Executar(){

        Verificadores verificar = new Verificadores();

        int limite = Prompt.lerInteiro("Digite quantas pessoas deseja analizar");

        for (int i = 0; i < limite; i++) {
            Prompt.separador();
            String nome = Prompt.lerLinha("Digite o nome");
            Prompt.imprimir("M - masculino   F - feminino");
            String sexo = Prompt.lerLinha("Digite o sexo");
            verificar.veriSexo(sexo);
        }

        Prompt.separador();
        Prompt.imprimir("Total de pessoas do sexo masculino " + verificar.getTotalM());
        Prompt.imprimir("Total de pessoas do sexo feminino  " + verificar.getTotalF());
        Prompt.separador();

    }

}