/*

23. Faça um programa que leia o nome, o sexo, a altura e a idade de uma pessoa. Calcule e mostre 
nome e o seu peso ideal de acordo com as seguintes características da pessoa: 

*/

import lib.Pessoa;
import lib.Prompt;

public class Exercicio23 {

    public static void Executar(){
        
        String nome = Prompt.lerLinha("Digite o nome");
        Prompt.imprimir("M - masculino  F - feminino");
        String sexo = Prompt.lerLinha("Digite o sexo");
        double altura = Prompt.lerDecimal("Digite a altura (em cm)");
        int idade = Prompt.lerInteiro("Digite a idade");

        Pessoa pessoa = new Pessoa();

        pessoa.setNome(nome);
        pessoa.EMC(sexo, altura, idade);

        Prompt.imprimir("O peso ideal para " + pessoa.getNome() + " é de " + pessoa.getEmc() + "Kg");



    }

}